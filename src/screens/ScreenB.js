import React, {Component} from 'react';
import {Text} from 'react-native';
import MainContainer from '../components/MainContainer';

export default class ScreenB extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <MainContainer navigate={navigate}>
      <Text>Screen B</Text>
      </MainContainer>
    );
  }
}
