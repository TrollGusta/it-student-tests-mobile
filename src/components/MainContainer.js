import React, {Component} from 'react';
import {Button, View, Text} from 'react-native';
import NavBar from './NavBar';

export default class MainContainer extends Component {

  render() {
    const {navigate} = this.props;
    return (
      <View>
        {this.props.children}
        <NavBar navigate = {navigate}/>
        <Text>Main text</Text>
      </View>
    );
  }
}
