import StartScreen from "../screens/StartScreen";
import ScreenA from "../screens/ScreenA";
import ScreenB from "../screens/ScreenB";

export default [
    {
        name: "Start",
        component: StartScreen,
        options: {},
    },
    {
        name: "ScreenA",
        component: ScreenA,
        options: {},
    },
    {
        name: "ScreenB",
        component: ScreenB,
        options: {},
    },
]
